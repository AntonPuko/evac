const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const webpack = require('webpack-stream');

gulp.task('html', function () {
  return gulp.src('./src/**/*.html')
    .pipe(gulp.dest('./dist/'))
});

gulp.task('html:watch', function () {
  gulp.watch('./src/**/*.html', ['html']);
});

gulp.task('img', function () {
  return gulp.src('./src/img/**/*.*')
    .pipe(gulp.dest('./dist/img/'))
});

gulp.task('img:watch', function () {
  gulp.watch('./src/img/**/*.*', ['img']);
});

gulp.task('sass', function () {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError)) //todo minification
    .pipe(gulp.dest('./dist/css'))
});

gulp.task('sass:watch', function () {
  gulp.watch('./src/sass/**/*.scss', ['sass']);
});

gulp.task('js', function () {
  return gulp.src('./src/js/index.js')
    .pipe(webpack({
      module: {
        loaders: [
          {test: /\.js$/, exclude: /node_modules/, loader: 'babel'}
        ]
      },
      output: {
        filename: 'bundle.js',
        library: 'maps'
      },

    }))
    .on('error', function handleError() {
      this.emit('end'); // Recover from errors
    })
    .pipe(gulp.dest('dist/js'))
});

gulp.task('js:watch', function () {
  gulp.watch('./src/js/**/*.js', ['js']);
});

gulp.task('build', ['sass', 'html', 'js', 'img']);



gulp.task('default', ['build', 'sass:watch', 'html:watch', 'js:watch', 'img:watch']);