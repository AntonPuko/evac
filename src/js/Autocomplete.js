export default class GoogleAutocomplete {
  constructor(inputElement, map) {
    this.autocomplete = new google.maps.places.Autocomplete(inputElement);
    this.autocomplete.bindTo('bounds', map);
  }

  onPlaceChanged(cb) {
    this.autocomplete.addListener('place_changed', function () {
      const place = this.getPlace();
      cb(place);
    });
  }
}