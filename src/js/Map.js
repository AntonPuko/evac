export default class GoogleMap {

  constructor(querySelector) {
    const mapDiv = document.querySelector(querySelector);
    this.map = new google.maps.Map(mapDiv, {
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      center: {
        lat: 54.704, //todo get from client navigator?
        lng: 20.486
      }
    });
  }

  addMarker = (label, position) => {
    return new google.maps.Marker({
      map: this.map,
      position,
      label: label,
      draggable: true
    });
  };

  expandViewportToFitPlace = (place) =>  {
    if (place.geometry.viewport) {
      this.map.fitBounds(place.geometry.viewport);
    } else {
      this.map.setCenter(place.geometry.location);
      this.map.setZoom(17);
    }
  };

  onClick = (cb) =>  {
    google.maps.event.addListener(this.map, 'click', (event) => cb(event));
  }
}