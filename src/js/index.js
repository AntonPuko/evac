import Directions from './Directions';
import Modal from './modal';
import GoogleMap from './Map';
import GoogleDirectionsService from './DirectionsService';
import GoogleAutoComplete from './Autocomplete';

function setDistanceToHtml(distance) {
  document.getElementById('distance-span').innerText = distance + ' км';
}

function setPriceToHtml(price) {
  document.getElementById('price-span').innerText = price + ' руб.';
}

export function onMapsLoad() {
  const fromInput = document.getElementById('evac-from-input');
  const toInput = document.getElementById('evac-to-input');
  const modal = new Modal();

  const gMap = new GoogleMap('.map');

  const directions = new Directions();
  let fromMarker = null;
  let toMarker = null;

  const addFromMarker = gMap.addMarker.bind(null, 'A');
  const addToMarker = gMap.addMarker.bind(null, 'B');

  const TRAVEL_MODE = google.maps.TravelMode.DRIVING;
  const service = new GoogleDirectionsService(gMap.map, TRAVEL_MODE);

  service.onDirectionsChanged((directions) => {
    const distance = computeTotalDistance(directions);
    const price = computePrice(distance);

    setDistanceToHtml(distance);
    setPriceToHtml(price);
  });

  directions.on(directions.eventsTypes.directionChanged, ({type}) => {
    if (type === directions.types.from) {
      if (!fromMarker) {
        fromMarker = addFromMarker(directions.from);

        google.maps.event.addListener(fromMarker, 'dragend', () => {
          directions.setFrom(fromMarker.getPosition());
        });
      }
    } else {
      if (!toMarker) {
        toMarker = addToMarker(directions.to);

        google.maps.event.addListener(toMarker, 'dragend', () => {
          directions.setTo(toMarker.getPosition());
        });
      }
    }

    if (directions.from && directions.to) {
      service.route(directions.from, directions.to, (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {

          fromInput.value = response.routes[0].legs[0].start_address;
          toInput.value = response.routes[0].legs[0].end_address;

          fromMarker.setPosition(response.routes[0].legs[0].start_location);
          toMarker.setPosition(response.routes[0].legs[0].end_location);
        } else {
          modal.show('маршрут не найден!');
        }
      });
    }
  });

  gMap.onClick((event) => {
    const location = event.latLng;

    if (!directions.from) {
      directions.setFrom(location);
    } else if (!directions.to) {
      directions.setTo(location);
    }
  });

  const fromAutocomplete = new GoogleAutoComplete(fromInput, gMap.map);
  fromAutocomplete.onPlaceChanged((place) => {
    if (!place.geometry) {
      modal.show('Место не найдено!');
      return;
    }

    gMap.expandViewportToFitPlace(place);
    directions.setFrom(place.geometry.location);
  });

  const toAutocomplete = new GoogleAutoComplete(toInput, gMap.map);
  toAutocomplete.onPlaceChanged((place) => {
    if (!place.geometry) {
      modal.show('Место не найдено!');
      return;
    }

    gMap.expandViewportToFitPlace(place);
    directions.setTo(place.geometry.location);
  });

  function computeTotalDistance(result) {
    let total = 0;
    const route = result.routes[0];
    for (let i = 0; i < route.legs.length; i++) {
      total += route.legs[i].distance.value;
    }

    return (total / 1000).toFixed(2);
  }

  function computePrice(distance) {
    return (distance * 500).toFixed(2);
  }
}