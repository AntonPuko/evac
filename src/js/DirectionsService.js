export default class GoogleDirectionsService {
  constructor(map, travelMode) {
    this.travelMode = travelMode;

    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
      polylineOptions: {
        strokeWeight: 4,
        strokeOpacity: 1,
        strokeColor: 'red'
      },
      map
    });
  }

  onDirectionsChanged(cb) {
    this.directionsDisplay.addListener('directions_changed', () => {
      cb(this.directionsDisplay.getDirections());
    });
  }

  route(fromPosition, toPosition, cb) {
    if (!fromPosition || !toPosition) return;

    this.directionsService.route({
      origin: fromPosition,
      destination: toPosition,
      travelMode: this.travelMode
    }, (response, status) => {

      if (status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      }

      cb(response, status);
    });
  }
}






