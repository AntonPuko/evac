import Emitter from './Emitter';

export default class Directions extends Emitter {
  types = {
    from: 'from',
    to: 'to'
  };

  from = null;
  to = null;

  eventsTypes = {
    directionChanged: 'direction_changed',
  };

  setFrom(from) {
    this.from = from;
    this.emit(this.eventsTypes.directionChanged, {type: this.types.from});
  };

  setTo(to) {
    this.to = to;
    this.emit(this.eventsTypes.directionChanged, {type: this.types.to});
  }
}
