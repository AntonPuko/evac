export default class Modal {
  constructor() {
    this.modal = this.createModalHTML();
    this.isShown = false;

    this.modal.onclick = () => {
        this.hide();
      };

    document.onkeydown = (e) => {
      const ENTER_KEYCODE = 13;

      if (e.keyCode === ENTER_KEYCODE && this.isShown)
        this.hide();
    };


  }

  createModalHTML(message) {
    const modalDiv = document.createElement('div');
    modalDiv.classList.add('modal');

    const modalMessageDiv = document.createElement('div');
    modalMessageDiv.classList.add('modal__message');

    modalDiv.appendChild(modalMessageDiv);
    document.body.appendChild(modalDiv);

    return modalDiv;
  }

  hide() {
    this.modal.style.display = 'none';
    this.isShown = false;
  };

  show(message) {
    this.modal.style.display = 'flex';
    this.modal.querySelector('.modal__message').innerHTML = message;
    this.isShown = true;
  };
}


